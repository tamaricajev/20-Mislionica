import { TokenType } from '@angular/compiler/src/ml_parser/lexer';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { TestTemplate, TestToken, TestTokenType } from 'src/app/utils/testtoken';
import { ClassesModel } from '../../models/classes.model';
import { SubjectModel } from '../../models/subject.model';
import { ClassesService } from '../../services/classes.service';
import { SubjectsService } from '../../services/subjects.service';
import { TestsService } from '../../services/tests.service';

@Component({
  selector: 'app-teacher-tests',
  templateUrl: './teachertests.component.html',
  styleUrls: ['./teachertests.component.css']
})
export class TeacherTestsComponent implements OnInit {

  public subjectsO! : Observable<SubjectModel[]>;

  public activeSubject : string = "";
  public subjects : SubjectModel[] =  [];
  public classes : ClassesModel[] = [];

  public test : TestTemplate[] = [];

  constructor(private testService: TestsService, private classService : ClassesService, private subjectService : SubjectsService) 
  { }

  ngOnInit(): void 
  {
      this.subjectsO = this.subjectService.getAllSubjectsByGrade(3);
      this.subjectsO.subscribe(data=>
        {
          this.subjects = data;
        });
  }

  showClassesForSubject(subjectID : string) : void
  {
    this.activeSubject = subjectID;
    this.classes = [];
    this.test = [];
    this.classService.getAllClassesByGrade(3).subscribe(
      data=>
      {
        data.forEach(item =>{
          if(item.subjectID === this.activeSubject)
          {
            this.classes.push(item);
          }
        })
      }
    );
  }

  AddTemplateToTest(template : string)
  {
    var testtemplate : TestTemplate = new TestTemplate();
    var startIndex = 0;
    for(var i = 0; i < template.length; ++i)
    {
      if(template[i] === '/')
      {
        testtemplate.tokens.push(new TestToken(template.substr(startIndex, i - startIndex),TestTokenType.Text))
        testtemplate.tokens.push(new TestToken(template.substr(i, 2),TestTokenType.Field));
        startIndex = i+2;
        ++i;
      }
      else if(template[i] === '#')
      {
        testtemplate.tokens.push(new TestToken(template.substr(startIndex, i - startIndex),TestTokenType.Text));
        testtemplate.tokens.push(new TestToken(template.substr(i, 1),TestTokenType.Input));
        startIndex = i+1;
      }
      else if(template[i] === '$')
      {
        testtemplate.tokens.push(new TestToken(template.substr(startIndex, i - startIndex),TestTokenType.Text));
        testtemplate.tokens.push(new TestToken(template.substr(i+2, 1),TestTokenType.RadioButton));
        i++;
        startIndex = i+2;
      }
      if( i === template.length-1)
      {
        testtemplate.tokens.push(new TestToken(template.substr(startIndex, i - startIndex +1),TestTokenType.Text));
      }
    }

    this.test.push(testtemplate);
  }

  updateToken(token : TestToken, event : KeyboardEvent) : void
  {
    token.value = (event.target as HTMLInputElement).value;
  }

  submitTest()
  {
    const testHash = this.parseTest();
    this.testService.addTest(testHash, this.activeSubject ,3);
    this.test = [];
  }

  change(token : TestToken, index : number)
  {
    this.test[index].answer = token;
  }

  parseTest() : string
  {
    var hash = "";
    this.test.forEach((task,index)=>{
      hash += index+1; 
      hash += ".";
      task.tokens.forEach(token =>{
        if(token.type !== TestTokenType.RadioButton)
        {
          hash+=token.value;
        }
      });
      hash+=":";
      task.tokens.forEach(token =>{
        if(token.type === TestTokenType.RadioButton)
        {
          hash+="$" + token.value;
        }
      });
      hash+="%" + task.answer.value;
      hash+=";";
    });
    return hash;
  }
}
/*/f Field
# input
$ radio button
*/