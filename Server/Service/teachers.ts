import * as uuid from "uuid"
import bcrypt from 'bcrypt';

import * as jwt from '../Utils/jwt'
import { User } from "./users"
import TeacherModel from '../Models/teachers'
import Teachers from "../Models/teachers";


export class Teacher extends User
{
    toJSON():object
    {
        return {
            id: this.id,
            fullName: this.fullName,
            username: this.username,
            email: this.email,
            password: this.password
        }
    };

    static async hashPassword(password)
    {
        const SALT_ROUNDS = 10;
        return await bcrypt.hash(password, SALT_ROUNDS);
    };

    static async getUserJWTByUsername(username:string) 
    {
        const user = await this.getUserByUsername(username);
        if (!user) {
          return null;
        }

        return jwt.generateJWT({
            _id: user.id,
            fullName: user.fullName,
            username: user.username,
            password: user.password,
            email: user.email,
            status: user.status,
            imgUrl: user.imgUrl
        });
    };

    static async getAllUsers()
    {
        const users = await TeacherModel.find({}).exec();
        return users;
    };

    static async getUserByUsername(username:string)
    {
        const user = await TeacherModel.findOne({ username: username }).exec();
        return user;
    };

    static async getTeacherByFullname(teachername:string)
    {
        const user = await TeacherModel.findOne({ fullName: teachername }).exec();
        return user;
    };

    static async getUserByEmail(email:string)
    {
        const user = await TeacherModel.findOne({ email: email }).exec();
        return user;
    };
    
    static async getUsersByStatus(status:string)
    {
        const users = await TeacherModel.find({ status:status }).exec();
        return users;
    };

    static async addNewUser(fullName:string, username:string, email:string, password:string, status:string="active")
    {
        const cPassword = await this.hashPassword(password);
        const newUser = new TeacherModel({ _id: uuid.v4(), fullName, username, email, password: cPassword, status });  
        await newUser.save();
        return Teacher.getUserJWTByUsername(username);
    };

    static async changeUserPassword(username:string, oldPassword:string, newPassword:string):Promise<object>
    {
        const user = await this.getUserByUsername(username);
        const matchPassword = await bcrypt.compare(oldPassword, user.password);

        if(!matchPassword)
        {
            return null;
        }
        
        const cPassword = await this.hashPassword(newPassword);
        await Teachers.findOneAndUpdate({ username: username },
                                        { $set: { password: cPassword } },
                                        { new: true });
        
        return Teacher.getUserJWTByUsername(username);
    };

    static async isUserPasswordValid(username, password)
    {
        const user = await this.getUserByUsername(username);
        if(!user)
        {
            return null;
        }
        return await bcrypt.compare(password, user.password);
    };

    static async deleteUser(username:string)
    {
        await TeacherModel.findOneAndDelete({ username: username }).exec();
    };

    static async changeProfileImage(username:string, imgUrl: string)
    {
        await Teachers.updateOne({ username: username },
            { $set: { imgUrl: imgUrl } });
    };

    static async updateUserData(fullname, username, email, newUsername) {
        await Teachers.updateOne({ username: username },
            { $set: { fullName: fullname, email: email, username: newUsername } });
    };
}
