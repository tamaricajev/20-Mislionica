export interface IJWTTokenStudentData {
  _id: string,
  fullName: string,
  username: string,
  teacher: string,
  grade: number,
  password: string,
  email: string,
  status: string,
  imgUrl: string
}

export interface IJWTTokenTeacherData {
  _id: string,
  fullName: string,
  username: string,
  teacher: string,
  grade: number,
  password: string,
  email: string,
  status: string,
  imgUrl: string
}
