export class User {
    constructor(public fullname: string = "", public username:string = "", public teachername : string | null = "", 
                public email: string = "", public password:string="", public grade:number, public imgUrl : string) {}
}