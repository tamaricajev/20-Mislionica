import express from "express"
import * as controller from '../../Controllers/test'

const router = express.Router();

router.post('/', controller.getUnsubmitedTests);
router.get('/template', controller.getTestTemplateForClass);
router.post('/add', controller.addTest);
router.post('/submit', controller.submitTets);

export default router;