import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ClassesModel } from '../../models/classes.model';
import { ClassesService } from '../../services/classes.service';
import { ShowClassInfo } from './classes/classes.component';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.css']
})
export class SubjectsComponent implements OnInit, OnDestroy {
  @ViewChild("maths") maths!: ElementRef;
  @ViewChild("language") language!: ElementRef;
  @ViewChild("indicator") indicator!: ElementRef;

  @Input() grade!: number;
  private sub!:Subscription;

  classInfoData:ShowClassInfo = {classNumber: 0, classType: ""};
  activeSub!: string;
  allClasses:ClassesModel[] = [];
  toChildCClasses:Number[] = [];
  classInfoDataUrlToShow: string = "";

  constructor(private classService: ClassesService) {
  }
  ngOnDestroy(): void {
    this.sub ? this.sub.unsubscribe() : null;
  }

  ngOnInit(): void {
    this.activeSub = "OS" + this.grade + "01"; // MathID on refresh
    this.sub = this.classService.getAllClassesByGrade(this.grade).subscribe((data:ClassesModel[]) => {
      data.forEach((aClass) => {
        this.allClasses.push(aClass);
      })
      this.toChildCClasses = this.allClasses.filter((aClass) => {return aClass.subjectID === this.activeSub}).map((aClass) => aClass.classNumber);
      this.toChildCClasses.sort().reverse();
    });
  }

  readClassInfo(childInfoClass: ShowClassInfo) {
    this.resetClassInfoData();
    this.classInfoData.classNumber = childInfoClass.classNumber;
    this.classInfoData.classType = childInfoClass.classType;

    this.updatePdfView();
  }

  swapSubject(activeSubject: number) : void
  {
    this.resetClassInfoData()
    let active;
    if(activeSubject)
    {
      this.activeSub = "OS" + this.grade + "02";
      active = this.language;
    }
    else
    {
      this.activeSub = "OS" + this.grade + "01";
      active = this.maths;
    }
    this.toChildCClasses = this.allClasses.filter((aClass) => {return aClass.subjectID === this.activeSub}).map((aClass) => aClass.classNumber);
    this.updatePdfView();

    let indi = 0;
    var newRound = document.createElement('div'),x,y;

    newRound.className = 'cercle';
    active.nativeElement.appendChild(newRound);

    newRound.style.left = x + "px";
    newRound.style.top = y + "px";
    newRound.className += " anim";

    let leftMargin = activeSubject ? 150 : 0;

    this.indicator.nativeElement.style.marginLeft = indi + leftMargin + 'px';

    setTimeout(function() {
      newRound.remove();
    }, 1200);
  };

  private updatePdfView(): void
  {
    const filterPdfUrl = this.allClasses.filter((aClass) => {
      return aClass.subjectID === this.activeSub && aClass.classNumber === this.classInfoData.classNumber;})
             .map((aClass) => { 
               if(this.classInfoData.classType === "showClass") { 
                  return aClass.materialsUrl; 
                } else if (this.classInfoData.classType === "showExercise") {
                  return aClass.exercisesUrl;
                } else { return aClass.classSummaryImgUrl; }
              })[0]
    this.classInfoDataUrlToShow = filterPdfUrl ? filterPdfUrl : "";
  }

  private resetClassInfoData(): void
  {
    this.classInfoData.classNumber = 0;
    this.classInfoData.classType = "";
  }
}
