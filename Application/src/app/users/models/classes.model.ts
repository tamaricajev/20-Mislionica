export class ClassesModel
{ 
    constructor(public _id: string, public subjectID: string, public classNumber : Number, public classTitle: string, public materialsUrl : string, public exercisesUrl : string, public  taskTemplate : string,
                public classSummaryImgUrl: string,
                public grade: number)
    {}
}