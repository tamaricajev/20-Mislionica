import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { JwtService } from "../../common/services/jwt.service";
import { map, catchError } from 'rxjs/operators';
import { TeacherModel } from '../models/teacher.model';

@Injectable({
  providedIn: 'root'
})

export class TeacherService {

  private teachers! : Observable<TeacherModel[]>;

  private readonly postTeacherImageUrl = "http://localhost:8000/api/teachers/profile-image/";
  private readonly postTeacher = "http://localhost:8000/api/teachers/update/";
  private readonly postTeacherPassword = "http://localhost:8000/api/teachers/updatePassword/";


  constructor(private auth: AuthenticationService, private http:HttpClient, private jwtService : JwtService) { }

  postTeacherProfileImage(file: File | null, username:string|undefined): Observable<any> | null {

    if(file == null) {
      return null;
    }

    const body: FormData = new FormData();
    body.append("file", file);

    return this.http.post<{ token: string }>(this.postTeacherImageUrl+username, body).pipe(
      catchError((error: HttpErrorResponse) => this.auth.handleError(error)),
      map((response: { token: string | null }) => this.auth.mapResponseToUser(response)));
  };

  public postUserData(fullname: string, username: string, email: string, newUsername : string): Observable<any> | null {
    const body = { fullname, email, newUsername };

    return this.http
      .post<{ token: string }>(this.postTeacher + username, body)
      .pipe(
        catchError((error: HttpErrorResponse) => this.auth.handleError(error)),
        map((response: { token: string | null }) => this.auth.mapResponseToUser(response))
      );
  };

  public postUserPassword(username: string, oldPassword: string, newPassword: string): Observable<any> | null {
    const body = { username, oldPassword, newPassword };

    return this.http
      .post<{ token: string }>(this.postTeacherPassword, body)
      .pipe(
        catchError((error: HttpErrorResponse) => this.auth.handleError(error)),
        map((response: { token: string | null }) => this.auth.mapResponseToUser(response))
      );
  };
  
}
