export class MarksModel
{ 
    constructor(public id: string, public isFinalMark : boolean, public mark : number,public semester: string ,public subjectID:string,
         public subjectNames: SubjectInfo, )
    {}
}

export class SubjectInfo
{
    constructor(public id: string, public grade: number,public subjectID: string,public subjectName: string){}
}