import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ClassesModel } from '../models/classes.model';

@Injectable({
  providedIn: 'root'
})
export class ClassesService {
  private readonly getClassImageUrl = 'http://localhost:8000/api/classes/class-image';
  private readonly getAllClassesByGradeUrl = 'http://localhost:8000/api/classes/'

  private Cclass! : Observable<ClassesModel>;
  private allClasses! : Observable<ClassesModel[]>;

  constructor(private http:HttpClient) { }

  // TODO: this should be used when we pushed all data in our database, because it's better to take the data that we need directly from DB.
  // for now, we are taking all data, and then filtering it on Application side.
  getClassInfoBySubjIDGradeAndClassNumber(subID:string, classNum: Number, grade:number) : Observable<ClassesModel>
  {
    const body ={subjectID: subID, classNumber: classNum, grade: grade};
    this.Cclass = this.http.post<ClassesModel>(this.getClassImageUrl, body);
    return this.Cclass;
  }

  getAllClassesByGrade(grade:number): Observable<ClassesModel[]>
  {
    this.allClasses = this.http.get<ClassesModel[]>(this.getAllClassesByGradeUrl + grade);
    return this.allClasses;
  }
}
