import {StudentModel} from "src/app/users/models/student.model"

export class TeacherModel
{
    public students: StudentModel[] =[];

    constructor(username: string, fullName: string)
    {
        this.students.push(new StudentModel(username, fullName));
    }
}