import * as marksService from "../Service/marks";

const getAllMarks = async (req, res, next) => {
    try 
    {
      const allMarks = await marksService.Marks.getAllMarks();
      res.status(200).json(allMarks);
    } 
    catch (error) 
    {
      next(error);
    }
  };

const getFinalMarks = async (req, res, next) => {
    try 
    {
        const finalMarks = await marksService.Marks.getFinalMarks();
        res.status(200).json(finalMarks);
    } 
    catch (error) 
    {
        next(error);
    }
};

const getMarksForStudent = async (req, res, next) => {
  const username = req.params.username;
  try 
  {
      const marks = await marksService.Marks.getMarksForStudent(username);
      res.status(200).json(marks);
  } 
  catch (error) 
  {
      next(error);
  }
};
const updateMarks = async (req, res, next) => {
  const username : string = req.body.username;
  const requests : marksService.marksChangeRequest[] = req.body.marksChangeRequests;
  try 
  {
    for(var i = 0; i < requests.length; ++i)
    {
      if(requests[i].changeTypeFlag === false)
      {
        await marksService.Marks.removeMarkForStudent(username,requests[i]);
      }
    }
    for(var i = 0; i < requests.length; ++i)
    {
      if(requests[i].changeTypeFlag === true)
      {
        await marksService.Marks.addMarkForStudent(username, requests[i]);
      }
    }
    res.status(201).json();
  } 
  catch (error) 
  {
    next(error);
  }
};


  export {
    getAllMarks,
    getFinalMarks,
    getMarksForStudent,
    updateMarks
};