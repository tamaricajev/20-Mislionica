import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const RequiredValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const inputValue = control.value.trim();

  if (inputValue.length === 0) {
    return { requiredValidator: { message: 'Обавезно поље није попуњено!' } };
  }

  return null;
};

export const FullNameValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const fullName: string[] = control.value.trim().split(' ').filter((namePart: string) => namePart.trim().length > 0);

  if (!fullName.every((namePart: string) => namePart.match(/^([A-Z][a-z]+)$/)) || fullName.length === 1) {
    return { fullNameValidator: { message: 'Унето име и презиме није у исправном облику. Исправан облик: Име Презиме.' } };
  }

  return null;
}

export const UserNameValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const username: string[] = control.value.trim().split(' ').filter((namePart: string) => namePart.trim().length > 0);

  if (username.length > 1) {
    return { usernamePartsValidator: { message: "Унето корисничко име мора да буде једна реч." } };
  }

  if (!username.every((part: string) => part.match(/^([a-z0-9_\.]+){6,}$/))) {
    return { usernameValidator: { message: "Унето корисничко име није у добром формату. Корисничко име сме да садржи само мала слова, специјалне карактере (. и _) и бројеве, а дужина мора бити минимум 6 карактера." } };
  }

  return null;
}

export const TeacherNameValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const fullName: string[] = control.value.trim().split(' ').filter((namePart: string) => namePart.trim().length > 0);

  if (!fullName.every((namePart: string) => namePart.match(/^([A-Z][a-z]+)$/)) || fullName.length === 1) {
    return { teachernameValidator: { message: 'Унето име и презиме учитеља није у исправном формату. Исправан формат: Име Презиме.' } };
  }

  return null;
}

export const EmailValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const email: string = control.value.trim();
  let correct: boolean = true;

  if (email.length != 0 && (email.indexOf("@") === -1 || email.lastIndexOf(".") === -1 || email.indexOf("@") > email.lastIndexOf("."))) {
    correct = false;
  }

  if (correct === false) {
    return { emailValidator: { message: "Унета имејл адреса није у добром формату." } };
  }

  return null;
}

export const PasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const password: string = control.value;

  if (password.match(/[\s]/)) {
    return { passwordBlankoValidator: { message: "Шифра не сме да садржи белине!" } };
  }

  if (password.length != 0 && !password.match(/(?=.*[A-Z])(?=.*[0-9])(?=.*[<>?,#@!\+-/%\*\.])[A-Za-z0-9<>?,#@!\+-/%\*\.]{5,}/)){
    return { passwordValidator: { message: `Унета шифра није у добром формату. Шифра мора да садржи:
    - Барем једно велико слово;
    - Мала слова;
    - Барем један број (0-9);
    - Барем један карактер од наведених (<>?,#@!+-/%*.);
    - Дужина шифре мора бити барем 5 карактера.` } };
  }

  return null;
}


