import * as subjectService from "../Service/subjects";

const getAllSubjects = async (req, res, next) => {
    try 
    {
      const allUsers = await subjectService.Subject.getAllSubjects();
      res.status(200).json(allUsers);
    } 
    catch (error) 
    {
      next(error);
    }
  };

const getSubjectsByGrade = async (req, res, next) => {
    const grade : number = req.params.grade;
    try 
    {
        const subjects = await subjectService.Subject.getSubjectsByGrade(grade);
        res.status(200).json(subjects);
    } 
    catch (error) 
    {
        next(error);
    }
};

const getSubjectsBySubjectID = async (req, res, next) => {
    const subjectID = req.params.subjectID;
    try 
    {
        const subject = await subjectService.Subject.getSubjectsBySubjectID(subjectID);
        res.status(200).json(subject);
    } 
    catch (error) 
    {
        next(error);
    }
};

  export {
      getAllSubjects,
      getSubjectsByGrade,
      getSubjectsBySubjectID
};