import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentTestsComponent } from './studenttests.component';

describe('StudentTestsComponent', () => {
  let component: StudentTestsComponent;
  let fixture: ComponentFixture<StudentTestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentTestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentTestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
