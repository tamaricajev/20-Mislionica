import * as uuid from "uuid"

import SubjectModule from '../Models/subjects'

export class Subject
{
    static async getAllSubjects():Promise<object>
    {
        const user = await SubjectModule.find({}).exec();
        return user;
    };

    static async getSubjectsByGrade(grade : number):Promise<object>
    {
        const user = await SubjectModule.find({grade : grade}).exec();
        return user;
    };

    static async getSubjectsBySubjectID(subjectID : string):Promise<object>
    {
        const user = await SubjectModule.find({subjectID : subjectID}).exec();
        return user;
    };
}