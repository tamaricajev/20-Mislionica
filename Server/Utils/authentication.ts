import { Student } from "../Service/students";
import { Teacher } from "../Service/teachers";
import { AppError } from './apperror';
import * as jwt from './jwt';

const canAuthenticateStudent = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  try 
  {
    if (!username || !password) 
    {
      const error = new AppError(400,`Унесите корисничко име и шифру.`);
      throw error;
    }

    const user = await Student.getUserByUsername(username);
    if (!user) 
    {
      const error = new AppError(404,`Корисник са корисничким именом "${username}" не постоји!`);
      throw error;
    }
    
    if (!(Student.isUserPasswordValid(username, password))) 
    {
      const error = new AppError(401,`Унели сте погрешну шифру за корисничко име "${username}"!`);
      throw error;
    }

    req.userId = user._id;
    req.username = user.username;

    next();
  } 
  catch (err) 
  {
    next(err);
  }
};

const canAuthenticateTeacher = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  try 
  {
    if (!username || !password) 
    {
      const error = new AppError(400,`Унесите корисничко име и шифру.`);
      throw error;
    }

    const user = await Teacher.getUserByUsername(username);
    if (!user) 
    {
      const error = new AppError(404,`Корисник са корисничким именом "${username}" не постоји!`);
      throw error;
    }
    
    if (!(Teacher.isUserPasswordValid(username, password))) 
    {
      const error = new AppError(401,`Унели сте погрешну шифру за корисничко име "${username}"!`);
      throw error;
    }

    req.userId = user._id;
    req.username = user.username;

    next();
  } 
  catch (err) 
  {
    next(err);
  }
};

const isAuthenticated = async (req, res, next) => {
  try 
  {
    const authHeader = req.header("Authorization");
    if (!authHeader) 
    {
      const error = new AppError(403,`Молимо Вас проследите неопходно заглавље за ауторизацију са вашим захтевом!`);
      throw error;
    }

    const token = authHeader.split(' ')[1];
    const decodedToken = jwt.verifyJWT(token);
    if (!decodedToken) 
    {
      const error = new AppError(401,`Аутенетификација није успела!`);
      throw error;
    }
    
    req.userId = decodedToken.id;
    req.username = decodedToken.username;

    next();
  } 
  catch (err) 
  {
    next(err);
  }
};

export {
  canAuthenticateStudent,
  canAuthenticateTeacher,
  isAuthenticated,
}
