import express from "express"
import mongoose from "mongoose"
import { urlencoded, json } from "body-parser"

import teachersRouter from './Routes/Api/teacher';
import studentsRouter from './Routes/Api/student';
import subjectsRouter from './Routes/Api/subjects';
import marksRouter from './Routes/Api/marks';
import testsRouter from './Routes/Api/test';
import classRouter from './Routes/Api/classes';
import { AppError } from "./Utils/apperror" 


const DB = process.env.DB_STING || 'mongodb://localhost:27017/mislionica';

mongoose.connect(DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

mongoose.connection.once('open', function () {
    console.log('[mongodb] Successfully connected!');
});

mongoose.connection.on('error', (error) => {
console.log('[mongodb] Error: ', error);
});

const app = express();

app.use(json());
app.use(urlencoded({ extended: false }));

app.use(function (req, res, next) 
{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  
    if (req.method === 'OPTIONS') 
    {
      res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PATCH, PUT, DELETE');
  
      return res.status(200).json({});
    }
  
    next();
});
  

app.use('/api/teachers', teachersRouter);
app.use('/api/students', studentsRouter);
app.use('/api/subjects', subjectsRouter);
app.use('/api/marks', marksRouter);
app.use('/api/tests', testsRouter);
app.use('/api/classes', classRouter);
app.use(express.static(__dirname));


app.use(function (req, res, next) 
{
    const error = new AppError(405, `Наведени захтев "${req.originalUrl}" није подржан!`);
    next(error);
});
  
app.use(function (error, req, res, next) {
const statusCode = error.status || 500;
res.status(statusCode).json({
    error: {
    message: error.message,
    status: statusCode,
    stack: error.stack,
    },
});
});  

export { app };
