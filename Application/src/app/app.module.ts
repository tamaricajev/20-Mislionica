import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginRegisterComponent } from './login-register/login-register.component';
import { LoginComponent } from './login-register/login/login.component';
import { RegisterComponent } from './login-register/register/register.component';
import { TeacherComponent } from './users/teacher/teacher.component';
import { StudentComponent } from './users/student/student.component';
import { SubjectsComponent } from './users/student/subjects/subjects.component';
import { StudentTestsComponent } from './users/student/studenttests/studenttests.component';
import { GradesComponent } from './users/student/grades/grades.component';
import { StudentProfileComponent } from './users/student/studentprofile/studentprofile.component';
import { SchedulerComponent } from './users/teacher/scheduler/scheduler.component';
import { RegistratorComponent } from './users/teacher/registrator/registrator.component';
import { TeacherProfileComponent } from './users/teacher/teacherprofile/teacherprofile.component';
import { TeacherTestsComponent } from './users/teacher/teachertests/teachertests.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainComponent } from './main/main.component';
import { ClassesComponent } from './users/student/subjects/classes/classes.component';
import { ClassInfoComponent } from './users/student/subjects/class-info/class-info.component';
import { SafePipe } from './users/pipes/safe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginRegisterComponent,
    LoginComponent,
    RegisterComponent,
    TeacherComponent,
    StudentComponent,
    SubjectsComponent,
    StudentTestsComponent,
    GradesComponent,
    StudentProfileComponent,
    SchedulerComponent,
    RegistratorComponent,
    TeacherProfileComponent,
    TeacherTestsComponent,
    MainComponent,
    ClassesComponent,
    ClassInfoComponent,
    SafePipe
  ],
  
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
