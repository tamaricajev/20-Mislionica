import express from "express"
import * as controller from '../../Controllers/students'
import * as auth from '../../Utils/authentication'

const router = express.Router();

router.post('/update/:username', controller.changeUserInfoData);
router.post('/updatePassword', controller.changeUserPassword);
router.post('/profile-image/:username', controller.changeProfileImage);
router.post('/login', auth.canAuthenticateStudent, controller.loginUser)
router.post('/register', controller.addNewUser);
router.get('/', controller.getAllUsers);
router.get('/:username', controller.getUserByUsername);
router.get('/status/:status', controller.getUsersByStatus);
router.get('/teacher/:teacher', controller.getUsersByTeacher);
router.get('/grade/:grade', controller.getUsersByGrade);
router.put('/', controller.changeUserPassword);
router.delete('/:username', controller.deleteUser);


export default router;