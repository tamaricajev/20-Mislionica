import mongoose from 'mongoose';

export interface IClassesDoc extends mongoose.Document
{
    _id: string,
    subjectID: string,
    classNumber : Number,
    classTitle: string,
    materialsUrl : string,
    exercisesUrl : string,
    taskTemplate : string,
    classSummaryImgUrl: string,
    grade: number
}

const classesShema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId, 
        required: true
    },
    subjectID: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    classNumber: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    classTitle: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    materialsUrl: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    exercisesUrl: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    taskTemplate: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    classSummaryImgUrl: {
        type: mongoose.Schema.Types.String,
        require: true,
        default: ""
    },
    grade: {
        type: mongoose.Schema.Types.Number,
        require: true,
    }
},    {
    versionKey: false
    });

const ClassModel = mongoose.model<IClassesDoc>('classes', classesShema);

export default ClassModel;
