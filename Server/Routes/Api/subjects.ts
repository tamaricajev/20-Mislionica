import express from "express"
import * as controller from '../../Controllers/subjects'

const router = express.Router();

router.get('/', controller.getAllSubjects);
router.get('/grade/:grade', controller.getSubjectsByGrade);
router.get('/subjectid/:subjectID', controller.getSubjectsBySubjectID);


export default router;