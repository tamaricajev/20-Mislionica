import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { User } from '../../models/user.model';
import { StudentService } from '../../services/student.service';
import { FullNameValidator, UserNameValidator, EmailValidator, PasswordValidator } from "../../../../app/login-register/validators/login-register.validator";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-student-profile',
  templateUrl: './studentprofile.component.html',
  styleUrls: ['./studentprofile.component.css']
})
 
export class StudentProfileComponent implements OnInit, OnDestroy {
  private subs:Subscription[] = [];

  studentForm !: FormGroup; 
  @Input() studentInfo !: User | null; 
  
  constructor(private studentService : StudentService) { }

  ngOnInit(): void { 
    this.studentForm = new FormGroup({
      fullname: new FormControl({ value: this.studentInfo?.fullname, disabled: true }, [Validators.required, FullNameValidator]),
      username: new FormControl({ value: this.studentInfo?.username, disabled: true }, [Validators.required, UserNameValidator]),
      email: new FormControl({ value: this.studentInfo?.email, disabled: true }, [Validators.required, EmailValidator]),
      password: new FormControl({value: "", disabled: true}, [Validators.required]),
      newPassword: new FormControl({value: "", disabled: true}, [Validators.required, PasswordValidator])
    });
  }

  ngOnDestroy(): void
  {
    this.subs.forEach((val, index) => this.subs[index] ? this.subs[index].unsubscribe() : null);
  }

  private imageToUpload : File | null = null;

  currentDate() : string {
    var utc = new Date().toJSON().slice(0,10).replace(/-/g,'.');
    var data = utc.substr(8);
    var month = utc.substr(5, 2);
    var year = utc.substr(0, 4);

    return data + '.' + month + '.' + year + '.';
  }

  private checkValidation() : boolean {
    let hasError: boolean = false;

    const errorFullname : ValidationErrors | null = this.studentForm.get("fullname")!.errors;
    const errorUsername : ValidationErrors | null = this.studentForm.get("username")!.errors;
    const errorEmail : ValidationErrors | null = this.studentForm.get("email")!.errors;
    const errorOldPassword : ValidationErrors | null = this.studentForm.get("password")!.errors;
    const errorNewPassword : ValidationErrors | null = this.studentForm.get("newPassword")!.errors;

    if (errorFullname != null) {
      if (errorFullname?.fullNameValidator){
        window.alert(errorFullname.fullNameValidator.message);
      }
      hasError = true;
    }

    if (errorUsername != null) {
      if (errorUsername?.usernamePartsValidator){
        window.alert(errorUsername.usernamePartsValidator.message);
      } 

      if (errorUsername?.usernameValidator){
        window.alert(errorUsername.usernameValidator.message);
      }

      hasError = true;
    }
    
   if (errorEmail != null) {
    if (errorEmail.emailValidator) {
      window.alert(errorEmail.emailValidator.message);
    }

     hasError = true;
   } 

  if (errorOldPassword != null) {

    if(errorOldPassword.required) {
      window.alert("Поље за унос старе шифре није попуњено.");
    }

    hasError = true;
  }

  if (errorNewPassword != null) {

    if(errorNewPassword.required) {
      window.alert("Поље за унос нове шифре није попуњено.");
    }

    if (errorNewPassword.passwordBlankoValidator){
      window.alert(errorNewPassword.passwordBlankoValidator.message);
    }

    if (errorNewPassword.passwordValidator){
      window.alert(errorNewPassword.passwordValidator.message);
    }

    hasError = true;
  } 

    return hasError;
  }

  grade() : string {
    var grade = "";

    if (this.studentInfo?.grade === 1) {
      grade = "први";
    } else if (this.studentInfo?.grade === 2) {
      grade = "други";
    } else if (this.studentInfo?.grade === 3) {
      grade = "трећи";
    } else {
      grade = "четврти";
    }
    
    return grade;
  }

  enableChangeFields() : void {
    this.studentForm.get("username")?.enable();
    this.studentForm.get("fullname")?.enable();
    this.studentForm.get("email")?.enable();
  }

  disableChangeFields() : void {
    this.studentForm.get("username")!.disable();
    this.studentForm.get("fullname")?.disable();
    this.studentForm.get("email")?.disable();
  }

  enableChangePassword() : void {
    this.studentForm.get("password")?.enable();
    this.studentForm.get("newPassword")?.enable();
  }

  disableChangePassword() : void {
    this.studentForm.get("password")?.disable();
    this.studentForm.get("newPassword")?.disable();
  }

  getProfileImage() : string | undefined {
    return this.studentInfo?.imgUrl;
  }

  public handleFileInput(event: Event): void {
    const files : FileList | null = (event.target as HTMLInputElement).files;
    if (!files!.length) {
      return;
    }
    this.imageToUpload = files!.item(0);

    this.subs.push(this.studentService.patchUserProfileImage(this.imageToUpload, this.studentInfo?.username)!.subscribe());
  }

  public onSubmitUserForm() : void {
    if (this.checkValidation() !== false) { return; }

    this.subs.push(this.studentService.postUserData(this.studentForm.get("fullname")?.value, 
                   this.studentInfo!.username, 
                   this.studentForm.get("email")?.value, 
                   this.studentForm.get("username")?.value)!
                   .subscribe((user : User) => {
                      this.studentForm.get("fullname")?.patchValue(user.fullname);
                      this.studentForm.get("username")?.patchValue(user.username);
                      this.studentForm.get("email")?.patchValue(user.email);
    }));
    this.disableChangeFields();
  }

  public onSubmitUserFormPassword() : void {
    if (this.checkValidation() !== false) { return; }

    this.subs.push(this.studentService.postUserPassword(this.studentForm.get("username")?.value, 
                   this.studentForm.get("password")?.value, 
                   this.studentForm.get("newPassword")?.value)!
                   .subscribe((user : User) => {
                      this.studentForm.get("newPassword")?.patchValue("");
                      this.studentForm.get("password")?.patchValue("");
    }));

    this.disableChangePassword();
  }
}
